console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	let printName = function printInfo(){
		let name = prompt("What is your name?");
		let age = prompt("How old are you?");
		let address = prompt("Where do you live?");
		alert("Thank you for your input!");

		console.log("Hello, " + name);
		console.log("You are " + age + " years old.");
		console.log("You live in " + address);
	}

	printName();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	let printFavoriteBand = function printMusicalArtists(){
		let artistA = "1. The Beatles";
		let artistB = "2. Metallica";
		let artistC = "3. The Eagles";
		let artistD = "4. L'arc~en~Ciel";
		let artistE = "5. Eraserheads";

		console.log(artistA);
		console.log(artistB);
		console.log(artistC);
		console.log(artistD);
		console.log(artistE);
	}

	printFavoriteBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	let printFavoriteMovies = function printWithRottenTomatoesRating(){
		let movieA = "1. The Godfather";
		let ratingA = "Rotten Tomatoes Rating: 97%";
		let movieB = "2. The Godfather, Part II";
		let ratingB = "Rotten Tomatoes Rating: 96%";
		let movieC = "3. Shawshank Redemption";
		let ratingC = "Rotten Tomatoes Rating: 91%";
		let movieD = "4. To Kill A Mockingbird";
		let ratingD = "Rotten Tomatoes Rating: 93%";
		let movieE = "5. Psycho";
		let ratingE = "Rotten Tomatoes Rating: 96%";

		console.log(movieA);
		console.log(ratingA);
		console.log(movieB);
		console.log(ratingB);
		console.log(movieC);
		console.log(ratingC);
		console.log(movieD);
		console.log(ratingD);
		console.log(movieE);
		console.log(ratingE);
	}

	printFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	let printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};


	// console.log(friend1);
	// console.log(friend2);

	printFriends();



